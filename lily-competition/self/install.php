<?php
require ('config.php');

try {
	$connection = new PDO($host, $username, $password, $options);
	$sql = file_get_contents('data/init.sql');
	$connection->exec($sql);
	
	echo 'Tables created successfully.';
} catch(PDOException $error) {
	echo $error->getMessage();
}

?>