<?php
include __DIR__ . '/vendor/autoload.php';
// \LilDew\MVC\Routing::setRouteFromUrl($_SERVER['SCRIPT_NAME'], $_SERVER['REDIRECT_URL']);
\LilDew\MVC\Routing::setRoute($_SERVER['PATH_INFO']);
$defaultRoute = 'LilDew\Competitie/Admin/Index';
\LilDew\MVC\Routing::init($defaultRoute);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Routing</title>
    <link rel="stylesheet" href="css/app.css" type="text/css"/>
</head>
<body>
<h1>Is de pagina die wordt opgeroepen als de bezoeker op onze Competition pagina komt</h1>
<pre id="feedback">
    <?php echo \LilDew\MVC\Routing::toString(); ?>
    <?php echo var_dump($_SERVER);?>
</pre>
</body>
</html>