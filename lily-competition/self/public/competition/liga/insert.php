<?php
require_once ('../../../config.php');
require_once ('../../../common.php');

$newLiga = array(
	"Name" => escape($_POST['Name']),
	"Year"  => escape($_POST['Year']),
	"IsInPlanning" => $_POST['IsInPlanning'] == '0' ? 0 : 1
);


$insertSql = "insert into Liga (Name, Year, IsInPlanning) values (:Name, :Year, :IsInPlanning)";

try {
    $connection = new PDO($host, $username, $password, $options);
    $statement = $connection->prepare($insertSql);
    $statement->execute($newLiga);
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}
header("Location: index.php");