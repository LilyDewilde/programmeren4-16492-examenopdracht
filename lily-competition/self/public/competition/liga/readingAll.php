<?php
require_once ('../../../config.php');
require_once ('../../../common.php');


try {
    $connection = new PDO($host, $username, $password, $options);
    
    $sqlSelect = "SELECT * from Liga";
    $statementReadingAll = $connection->prepare($sqlSelect);
    $statementReadingAll->execute();
    $resultReadingAll = $statementReadingAll->fetchAll();
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}                           


if ($resultReadingAll && $statementReadingAll->rowCount() > 0) { 
?>
        <table>
            <thead>
                <tr>
                    
                    <th>Select</th>
                    <th>Name</th>
                    <th>Year</th>
                    <th>Is in planning</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultReadingAll as $row) { 
	?>
                <tr>
                    <td><a class="icon-arrow-right" href="readingOne.php?Id=<?php echo escape($row['Id']); ?>"><span class="screen-reader-text">Lees rij</span></a></td>
                    <td><?php echo escape($row["Name"]); ?></td>
                    <td><?php echo escape($row["Year"]); ?></td>
                    <td><?php 
                    if (escape($row["IsInPlanning"]) == 1) echo yes;
                    else if (escape($row["IsInPlanning"]) == 0) echo no;
                    else echo error;
                    ?></td>

                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } else { ?>
        <blockquote>Geen ligas gevonden.</blockquote>
        <?php 
        } 
        
  ?>
  