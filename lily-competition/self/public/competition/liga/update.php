<?php
require_once ('../../../config.php');
require_once ('../../../common.php');
$updateLiga = array(
"Id" => escape($_POST['Id']),
"Name" => escape($_POST['Name']),
"Year"  => escape($_POST['Year']),
"IsInPlanning" => escape($_POST['IsInPlanning'] == '0' ? false : true));


$sql = "UPDATE Liga set Name = :Name, Year = :Year, IsInPlanning = :IsInPlanning WHERE Id = :Id";

try {
    $connection = new PDO($host, $username, $password, $options);
    $statement = $connection->prepare($sql);
    $statement->execute($updateLiga);
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}
header("Location: readingOne.php?Id={$updateLiga['Id']}");