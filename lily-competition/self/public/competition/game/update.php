<?php
require_once ('../../../config.php');
require_once ('../../../common.php');
$updateGame = array(
"Id" => escape($_POST['Id']),
"Date" => escape($_POST['Date']),
"Time" => escape($_POST['Time']),
"Status"  => escape($_POST['Status']),
"ScoreHome"  => escape($_POST['ScoreHome']),
"ScoreVisitors"  => escape($_POST['ScoreVisitors'])
);


$sql = "UPDATE Game set Date = :Date, Time = :Time, Status = :Status, ScoreHome = :ScoreHome, ScoreVisitors = :ScoreVisitors WHERE Id = :Id";

try {
    $connection = new PDO($host, $username, $password, $options);
    $statement = $connection->prepare($sql);
    $statement->execute($updateGame);
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}
header("Location: readingOne.php?Id={$updateGame['Id']}");