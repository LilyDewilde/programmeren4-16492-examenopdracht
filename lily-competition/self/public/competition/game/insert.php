<?php
require_once ('../../../config.php');
require_once ('../../../common.php');

$newGame = array(
	"Date" => escape($_POST['Date']),
	"Time" => escape($_POST['Time']),
	"Status"  => escape($_POST['Status']),
	"ScoreHome"  => escape($_POST['ScoreHome']),
	"ScoreVisitors"  => escape($_POST['ScoreVisitors'])
);


$insertSql = "insert into Game (Date, Time, Status, ScoreHome, ScoreVisitors) values (:Date, :Time, :Status, :ScoreHome, :ScoreVisitors)";

try {
    $connection = new PDO($host, $username, $password, $options);
    $statement = $connection->prepare($insertSql);
    $statement->execute($newGame);
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}
header("Location: index.php");