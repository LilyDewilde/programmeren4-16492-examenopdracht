<?php
require_once ('../../../config.php');
require_once ('../../../common.php');

try {
    $connection = new PDO($host, $username, $password, $options);
    
    $sqlSelect = "SELECT Id, Name FROM Team";
    $statementReadingAll = $connection->prepare($sqlSelect);
    $statementReadingAll->execute();
    $teamList = $statementReadingAll->fetchAll();
    echo 'aantal rijen in team ' . $statementReadingAll->rowCount();
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}                           

include ('../../templates/header.php'); 
?>
<main>
    <article>
        <header class="command-bar">
            <h2>Liga</h2>
            <nav>
                <button type="submit" value="insert" form="form" name="submit" class="icon-floppy-disk"><span class="screen-reader-text">Insert</span></button></a>
                <a class="icon-cross" href="index.php"><span class="screen-reader-text">Cancel</span></a>
            </nav>
        </header>
             <!-- form>(label+input:text)*6 -->
            <form action="insert.php" method="post" id="form">
               <fieldset>
                    <div>
                        <label for="FirstName">Voornaam</label>
                        <input type="text" name="FirstName" id="FirstName" required>
                    </div>
                    <div>
                        <label for="LastName">Familienaam</label>
                        <input type="text" name="LastName" id="LastName" required>
                    </div>
                    <div>
                        <label for="email">E-mail</label>
                        <input type="email" name="Email" id="Email">
                    </div>
                    <div>
                        <label for="Address1">Adres 1</label>
                        <input type="text" name="Address1" id="Address1">
                    </div>
                    <div>
                        <label for="Address2">Adres 2</label>
                        <input type="text" name="Address2" id="Address2">
                    </div>
                    <div>
                        <label for="PostalCode">Postcode</label>
                        <input type="text" name="PostalCode" id="PostalCode">
                    </div>
                    <div>
                        <label for="City">Stad</label>
                        <input type="text" name="City" id="City">
                    </div>
                     <div>
                        <label for="Country">Land</label>
                        <input type="text" name="Country" id="Country">
                    </div>   
                     <div>
                        <label for="Phone">Tel</label>
                        <input type="text" name="Phone" id="Phone">
                    </div>
                   <div>
                        <label for="Birthday">Geboortedatum</label>
                        <input type="date" name="Birthday" id="Birthday" required>
                    </div>
                    <div>
                        <label for="TeamId">Team</label>
                        <select id="TeamId" name="TeamId">
                            <option style="display:none;"></option>
                            <?php
                                if ($teamList) {
                                    foreach ($teamList as $teamRow) {
                            ?>
                                    <option value="<?php echo $teamRow['Id'];?>">
                                        <?php echo $teamRow['Name'];?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
            </fieldset>
        </form>
        <div id="feedback">
            <?php 
               if (isset($_POST['submit']) && $statement) {
                    echo $newPlayer['Name'] . ' is toegevoegd.<br/>';
                }
                echo $sqlErrorMessage;
            ?>
        </div>
    </article>
    <aside>
        <?php include('readingAll.php');?>
    </aside>
    </main>
    
<?php include ('../../templates/footer.php'); ?>