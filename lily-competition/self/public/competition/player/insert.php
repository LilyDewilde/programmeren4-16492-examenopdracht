<?php
require_once ('../../../config.php');
require_once ('../../../common.php');


$newPlayer = array(
	"FirstName" => escape($_POST['FirstName']),
	"LastName"  => escape($_POST['LastName']),
	"Email"     => escape($_POST['Email']),
	"Address1"  => escape($_POST['Address1']),
	"Address2"  => escape($_POST['Address2']),
   	"PostalCode"  => escape($_POST['PostalCode']),
   	"City"  => escape($_POST['City']),
   	"Country"  => escape($_POST['Country']),
   	"Phone"  => escape($_POST['Phone']),
   	"Birthday"  => escape($_POST['Birthday']),
   	"TeamId"  => escape($_POST['TeamId'])
);

$insertSql = "insert into Player (FirstName, LastName, Email, Address1, Address2, PostalCode, City, Country, Phone, Birthday, TeamId) values (:FirstName, :LastName, :Email, :Address1, :Address2, :PostalCode, :City, :Country, :Phone, :Birthday, :TeamId)";

try {
    $connection = new PDO($host, $username, $password, $options);
    $statement = $connection->prepare($insertSql);
    $statement->execute($newPlayer);
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}
header("Location: index.php");