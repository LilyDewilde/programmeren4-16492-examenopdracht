<?php
require_once ('../../../config.php');
require_once ('../../../common.php');


    $newTeam = array(
    	"Name" => escape($_POST['Name']),
    	"Location"  => escape($_POST['Location']),
    	"Score"     => escape($_POST['Score'])
    	
    );

    $insertSql = "insert into Team (Name, Location, Score) values (:Name, :Location, :Score)";
 

    try {
        $connection = new PDO($host, $username, $password, $options);
        $statement = $connection->prepare($insertSql);
        $statement->execute($newTeam);
    } catch (PDOException $e) {
        $sqlErrorMessage = "Er is iets fout gelopen: {$e->getMessage()}";
    }

header("Location: index.php");