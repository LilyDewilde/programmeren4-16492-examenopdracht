<?php 
require_once ('../../../config.php');
require_once ('../../../common.php');

try {
    $id = $_GET['Id'];
    $connection = new PDO($host, $username, $password, $options);
    
     $sqlSelect = "SELECT * from Team WHERE Id = :Id";
    $statement = $connection->prepare($sqlSelect);
    $statement->bindParam(':Id', $id, PDO::PARAM_INT);
    $statement->execute();
    $result = $statement->fetch(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}                           




include ('../../templates/header.php'); ?>


<main>
    <article>
        <header class="command-bar">
            <h2>Team</h2>
            <nav>
                <a class="icon-pencil" href="updatingOne.php?Id=<?php echo escape($result['Id']);?>"><span class="screen-reader-text">Updating</span></a>
                <a class="icon-plus" href="insertingOne.php"><span class="screen-reader-text">Inserting</span></a>
              <a class="icon-bin" href="delete.php?Id=<?php echo escape($result['Id']);?>"><span class="screen-reader-text">Updating</span></a>
                <a class="icon-cross" href="index.php"><span class="screen-reader-text">Cancel</span></a>



            </nav>
        </header>
        <fieldset>
            <div id="feedback">
  <?php if (isset($_POST['submit']) && $statement) {
    echo $newTeam['Team'] . ' is toegevoegd';
}
?>

            </div>
            <!-- form>(label+input:text)*6 -->
            <form action="" method="post" id="">
                <input type="hidden" name="Id" id="Id" value="<?php echo $result['Id'];?>"/>
                <div>
                    <label for="Name">Name</label>
                    <input type="text" name="Name" id="Name" readonly
                        value="<?php echo $result['Name'];?>">
                </div>
                <div>
                    <label for="Location">Location</label>
                    <input type="text" name="Location" id="Location" readonly
                        value="<?php echo $result['Location'];?>">
                </div>
                <div>
                    <label for="Score">Score</label>
                    <input type="text" name="Score" id="Score" readonly
                        value="<?php echo $result['Score'];?>">
               </div>
            </form>

        </fieldset>
    </article>
    <aside>
        <?php include('readingAll.php');?>
    </aside>
</main>
<?php include ('../../templates/footer.php'); ?>


