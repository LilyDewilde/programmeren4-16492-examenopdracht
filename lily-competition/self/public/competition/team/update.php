<?php
require_once ('../../../config.php');
require_once ('../../../common.php');
$updateTeam = array(
"Id" => escape($_POST['Id']),
"Name" => escape($_POST['Name']),
"Location" => escape($_POST['Location']),
"Score" => escape($_POST['Score']));


$sql = "UPDATE Team set Name = :Name, Location = :Location, Score = :Score WHERE Id = :Id";

try {
    $connection = new PDO($host, $username, $password, $options);
    $statement = $connection->prepare($sql);
    $statement->execute($updateTeam);
} catch (PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}
header("Location: readingOne.php?Id={$updateTeam['Id']}");