<?php
if (isset($_POST['submit'])) {
	require "../config.php";
	require "../common.php";

	try {
		$connection = new PDO($host, $username, $password, $options);
		$new_user = array(
			//escaping is completely pointless when using, and strictly inferior to, prepared statements,but might as well go with the flow here
			"firstname" => escape($_POST['firstname']),
			"lastname"  => escape($_POST['lastname']),
			"email"     => escape($_POST['email']),
			"age"       => escape($_POST['age']),
			"location"  => escape($_POST['location'])
		);
		$sql = sprintf(
			"INSERT INTO %s (%s) values (%s)",
			"users",
			implode(", ", array_keys($new_user)),
			":" . implode(", :", array_keys($new_user))
		);
		$statement = $connection->prepare($sql);
		$statement->execute($new_user);
	} catch(PDOException $error) {
		echo $error->getMessage();
	}	
}
?>
<?php include "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) {
    echo $new_user['firstname'] . ' ' . $new_user['lastname'] . ' is toegevoegd';
}
?>

<form method="post">
	<label for="firstname">First Name</label>
	<input type="text" name="firstname" id="firstname">
	<label for="lastname">Last Name</label>
	<input type="text" name="lastname" id="lastname">
	<label for="email">Email Address</label>
	<input type="text" name="email" id="email">
	<label for="age">Age</label>
	<input type="text" name="age" id="age">
	<label for="location">Location</label>
	<input type="text" name="location" id="location">
	<input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Back to home</a>

<?php include "templates/footer.php"; ?>